package perlingeneration;


/**
 *
 * @author Hammer
 */
public class PerlinGeneration {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MapGeneratorFrame mgf = new MapGeneratorFrame();
        mgf.setDefaultCloseOperation(3);
        mgf.setVisible(true);
    }
    
}
