package perlingeneration;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Hammer
 */
public class MapView extends Canvas {

    private final int width;
    private final int height;
    private float[][] map;
    private final int sizeMult;
    private MapPanel mp;

    public MapView(int size, Object map, int sizemult) {
        this.width = size;
        this.height = size;
        this.map = (float[][]) map;
        this.sizeMult = sizemult;
    }

    class MapPanel extends JPanel {
        
        @Override
        public void paint(Graphics g) {
            super.paint(g);
            DrawHeight(g);
        }

        private void DrawHeight(Graphics g) {
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    paintRect(x, y, g);
                }
            }
        }

        private void paintRect(int x, int y, Graphics g) {
            int intensity = (int) (map[x][y] * 255);
            if (intensity > 255) { //Does not get used if you dont multiply intensity.
                intensity = 255;
            }
            
            if (intensity < 50) {
                //Water
                g.setColor(new Color(0, 0, intensity));
                g.fillRect(x * sizeMult, y * sizeMult, 1 * sizeMult, 1 * sizeMult);
            } else if (intensity < 55) {
                //Sand
                g.setColor(new Color(intensity, intensity, 0));
                g.fillRect(x * sizeMult, y * sizeMult, 1 * sizeMult, 1 * sizeMult);
            } else if (intensity < 100) {
                //Grass
                g.setColor(new Color(0, intensity, 0));
                g.fillRect(x * sizeMult, y * sizeMult, 1 * sizeMult, 1 * sizeMult);
            } else {
                //Mountain
                g.setColor(new Color(intensity, intensity, intensity));
                g.fillRect(x * sizeMult, y * sizeMult, 1 * sizeMult, 1 * sizeMult);
            }
        }
    }

    public void UpdateMap(float[][] map) {
        this.map = map;
        this.revalidate();
        this.repaint();
        mp.repaint();
    }

    public void view() {
        JFrame frame = new JFrame();
        frame.setSize(width, height);

        mp = new MapPanel();
        frame.add(mp);
        frame.setDefaultCloseOperation(3);
        frame.setSize(width * sizeMult, height * sizeMult+38);
        frame.setVisible(true);

    }
}
